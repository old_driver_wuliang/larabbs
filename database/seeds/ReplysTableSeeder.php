<?php

use Illuminate\Database\Seeder;
use App\Models\Reply;
use App\Models\User;
use App\Models\Topic;

class ReplysTableSeeder extends Seeder
{
    public function run()
    {
        // 获取所有用户 ID 数组 如：[1,2,3,4]
        $userIds = User::all()->pluck('id')->toArray();

        // 获取所有话题 ID 数组 如：[1,2,3,4]
        $topicIds = Topic::all()->pluck('id')->toArray();

        // 获取 Faker 实例
        /** @var Faker\Generator $faker */
        $faker = app(Faker\Generator::class);

        /** @var Reply $replys */
        $replys = factory(Reply::class)->times(1000)->make()
            ->each(function (Reply $reply, $index) use ($userIds, $topicIds, $faker) {
            $reply->user_id = $faker->randomElement($userIds);
            $reply->topic_id = $faker->randomElement($topicIds);
        });

        // 将数据集合转换为数组，并插入到数据库中
        (new Reply())->insert($replys->toArray());
    }

}

