@extends('layouts.app')
@section('title', $user->name . '修个个人信息')

@section('content')
    <div class="container">
        <div class="col-md-10 col-md-offset-1 panel panel-default">

            <div class="panel-heading">
                <h4>
                    <i class="glyphicon glyphicon-edit">编辑个人资料</i>
                </h4>
            </div>

            @include('common.error')

            <div class="panel-body">
                <form action="{{ route('users.update', $user->id) }}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <label for="name-field">用户名：</label>
                        <input id="name-field" type="text" name="name" value="{{ old('name', $user->name) }}" class='form-control'>
                    </div>

                    <div class="form-group">
                        <label for="email-field">邮 箱：</label>
                        <input id="email-field" type="email" name="email" value="{{ old('email', $user->email) }}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="introduction-field">个人简介：</label>
                        <textarea id="introduction-field" name="introduction" rows="3" class="form-control">{{ old('introduction', $user->introduction) }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="avatar-field" class="aratar-label">用户头像：</label>
                        <input id="avatar-field" type="file" name="avatar">

                        @if ($user->avatar)
                            <img class="thumbnail img-responsive" src="{{ $user->avatar }}" alt="用户头像" width="200px">
                        @endif
                    </div>

                    <div class="well well-sm">
                        <button type="submit" class="btn btn-primary btn-block">保 存</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
@stop