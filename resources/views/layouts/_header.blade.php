<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">LaraBBS</a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                <li class="{{ active_class(if_route('topics.index')) }}">
                    <a href="{{ route('topics.index') }}">话题</a>
                </li>
                <li class="{{ active_class(if_route('categories.show') && if_route_param('category', 1)) }}">
                    <a href="{{ route('categories.show', 1) }}">分享</a>
                </li>
                <li class="{{ active_class(if_route('categories.show') && if_route_param('category', 2)) }}">
                    <a href="{{ route('categories.show', 2) }}">教程</a>
                </li>
                <li class="{{ active_class(if_route('categories.show') && if_route_param('category', 3)) }}">
                    <a href="{{ route('categories.show', 3) }}">问答</a>
                </li>
                <li class="{{ active_class(if_route('categories.show') && if_route_param('category', 4)) }}">
                    <a href="{{ route('categories.show', 4) }}">公告</a>
                </li>
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @guest
                    <li><a href="{{ route('login') }}">登录</a></li>
                    <li><a href="{{ route('register') }}">注册</a></li>
                @else
                    {{--发布话题按钮--}}
                    <li>
                        <a href="{{ route('topics.create') }}">
                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                        </a>
                    </li>

                    {{--消息通知标志--}}
                    <li>
                        <a href="{{ route('notifications.index') }}" class="notifications-bdage" style="margin-top: -20x;">
                            <span class="badge badge-{{ Auth::user()->notification_count > 0 ? 'hint' : 'fade' }}" title="消息提醒">
                                {{ Auth::user()->notification_count }}
                            </span>
                        </a>
                    </li>

                    @include('layouts._userinfo_nav')
                @endguest
            </ul>
        </div>
    </div>
</nav>