@extends('layouts.app')
@section('title', '我的通知')

@section('content')
    <div class="container">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3 class="text-center">
                        <span class="glyphicon glyphicon-bell" aria-hidden="true"></span> 我的通知
                    </h3>

                    @if($notifications->count())
                        <div class="notification-list">
                            @foreach($notifications as $notification)
                                {{-- App\Notifications\TopicReplied 在class_basename方法中会取到 TopicReplied --}}
                                {{-- TopicReplied 经过 snake_case 方法会格式化为 topic_replied --}}
                                @include('notifications.types._' . snake_case(class_basename($notification->type)))
                            @endforeach
                            {{--渲染分页--}}
                            {!! $notifications->render() !!}
                        </div>
                    @else
                        <div class="empty-block">没有消息通知！</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection