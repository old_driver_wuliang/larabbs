@extends('layouts.app')
@section('title', $topic->id ? '编辑话题' :'新建话题')

@section('style')
    <link rel="stylesheet" href="{{ asset('css/simditor.css') }}">
@endsection

@section('content')
    <div class="container">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2 class="text-center">
                        @if($topic->id)
                            <i class="glyphicon glyphicon-edit"></i>
                            编辑话题
                        @else
                            <i class="glyphicon glyphicon-plus"></i>
                            新建话题
                        @endif
                    </h2>

                    <hr>

                    @include('common.error')

                    @if($topic->id)
                        <form action="{{ route('topics.update', $topic->id) }}" method="POST" accept-charset="utf-8">
                            <input type="hidden" name="_method" value="PUT">
                    @else
                        <form action="{{ route('topics.store') }}" method="POST" accept-charset="utf-8">
                    @endif
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <input class="form-control" type="text" name="title" value="{{ old('title', $topic->title) }}" placeholder="请输入标题" required >
                            </div>

                            <div class="form-group">
                                <select class="form-control" name="category_id" required id="category_id">
                                    <option value="" hidden disabled {{ $topic->id ? '' : 'selected' }}>请选择分类</option>
                                    @foreach ($categories as $value)
                                        <option value="{{ $value->id }}" {{ $topic->category_id == $value->id ? 'selected' : '' }}>{{ $value->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <textarea class="form-control" name="body" rows="3" placeholder="请输入至少三个字符的内容" required id="editor">{{ old('body', $topic->body) }}</textarea>
                            </div>

                            <div class="well well-sm">
                                <button type="submit" class="btn btn-primary btn-block">
                                    <span class="glyphicon glyphicon-ok" aria-hidden="true">保存</span>
                                </button>
                            </div>

                        </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('js/module.js') }}"></script>
    <script src="{{ asset('js/hotkeys.js') }}"></script>
    <script src="{{ asset('js/uploader.js') }}"></script>
    <script src="{{ asset('js/simditor.js') }}"></script>

    <script>
        $(document).ready(function () {
            var editor = new Simditor({
                textarea: $("#editor"),
                upload: {
                    url: "{{ route('topics.upload_image') }}",  // 请求接口地址
                    params: {_token: "{{ csrf_token() }}"},     // 表单提交的参数，Laravel 的 POST 请求必须带防止 CSRF 跨站请求伪造的 _token 参数
                    fileKey: 'upload_file',                     // 服务端获取上传图片的键值
                    connectionCount: 3,                         // 最多只能同时上传的图片个数
                    leaveConfirm: '文件上传中，关闭此页面将取消上传。'// 上传过程中，用户关闭页面时的提示
                },
                pasteImage: true // 支持图片粘贴上传
            });
        });
    </script>
@stop