@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">用户注册</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">用户名：</label>

                            <div class="col-md-6">
                                <input id="name" type="text" name="name" value="{{ old('name') }}" class="form-control" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">邮  箱：</label>

                            <div class="col-md-6">
                                <input id="email" type="email" name="email" value="{{ old('email') }}" class="form-control" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">密  码：</label>

                            <div class="col-md-6">
                                <input id="password" type="password" name="password" class="form-control" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">确认密码：</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" name="password_confirmation" class="form-control" required>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('captcha') ? ' has-error'  : ''}}">
                            <label for="captcha" class="col-md-4 control-label">验证码：</label>

                            <div class="col-md-6">
                                <input id="captcha" type="text" name="captcha" class="form-control">

                                <img class="thumbnail captcha" src="{{ captcha_src('flat') }}" alt="验证码" title="点击获取图片验证码" onclick="this.src='/captcha/flat?'+Math.random()">

                                @if ($errors->has('captcha'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('captcha') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary btn-block">
                                注册<i class="glyphicon glyphicon-arrow-right"></i>
                            </button>
                        </div>

                    </form>
                </div>

            </div>

        </div>
    </div>
</div>
@endsection
