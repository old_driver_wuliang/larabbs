<?php

namespace App\Handlers;

use GuzzleHttp\Client;
use Overtrue\Pinyin\Pinyin;

class SlugTranslateHandler
{
    /** @var string $apiurl 百度翻译接口地址 */
    private $apiurl = null;
    private $appId = null;
    private $appKey = null;
    private $from = null;
    private $to = null;

    /**
     * SlugTranslateHandler constructor.
     *
     * @param string $from
     * @param string $to
     */
    public function __construct($from = 'zh', $to= 'en')
    {
        $this->setFrom($from);
        $this->setTo($to);
        $this->setApiurl(config('services.baidu_translate.url'));
        $this->setAppKey(config('services.baidu_translate.key'));
        $this->setAppId(config('services.baidu_translate.appid'));
    }

    /**
     * 掉用百度翻译接口，将文本由指定的语言翻译成指定的语言 如：中文翻译成英文
     *
     * @author: wuliang@koudailc.com
     *
     * @param $text
     *
     * @return string
     */
    public function translate($text)
    {
        // 如果没有百度翻译APPID和APPKEY，则使用拼音代替
        if (empty($this->getAppId()) || empty($this->getAppKey())) {
            return $this->pinyin($text);
        }

        // 实例化 HTTP 客户端
        $client = new Client();
        $response = $client->get($this->str_query($text));
        $result = json_decode($response->getBody(), true);

        // 获取翻译结果
        if (isset($result['trans_result'][0]['dst'])) {
            return str_slug($result['trans_result'][0]['dst']);
        } else {
            // 如果百度翻译没有结果，使用拼音作为后备计划。
            return $this->pinyin($text);
        }
    }

    /**
     * 生成百度翻译接口请求参数
     *
     * @author: wuliang@koudailc.com
     *
     * @param $text
     *
     * @return string
     */
    private function str_query($text)
    {
        $salt = time();
        // 根据文档，生成 sign
        // http://api.fanyi.baidu.com/api/trans/product/apidoc
        // appid+q+salt+密钥 的MD5值
        $sign = md5($this->getAppId(). $text . $salt . $this->getAppKey());

        // 构建请求参数
        $query = http_build_query([
            'q'     => $text,
            'from'  => 'zh',
            'to'    => 'en',
            'appid' => $this->getAppId(),
            'salt'  => $salt,
            'sign'  => $sign,
        ]);

        return $this->getApiurl() . $query;
    }

    /**
     * 将汉字转为拼音
     *
     * @author: wuliang@koudailc.com
     *
     * @param string $text
     *
     * @return string
     */
    public function pinyin($text)
    {
        return str_slug(app(Pinyin::class)->permalink($text));
    }


    /**
     * @return string
     */
    public function getApiurl()
    {
        return $this->apiurl;
    }

    /**
     * @param string $apiurl
     */
    public function setApiurl($apiurl)
    {
        $this->apiurl = $apiurl;
    }

    /**
     * @return string
     */
    public function getAppId()
    {
        return $this->appId;
    }

    /**
     * @param string $appId
     */
    public function setAppId($appId)
    {
        $this->appId = $appId;
    }

    /**
     * @return string
     */
    public function getAppKey()
    {
        return $this->appKey;
    }

    /**
     * @param string $appKey
     */
    public function setAppKey($appKey)
    {
        $this->appKey = $appKey;
    }

    /**
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param string $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return string
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param string $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }
}