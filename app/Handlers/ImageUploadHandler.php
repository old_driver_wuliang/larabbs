<?php

namespace App\Handlers;

use Illuminate\Http\UploadedFile;
use Intervention\Image\Constraint;
use Intervention\Image\ImageManagerStatic as Image;

class ImageUploadHandler
{
    /** @var array 允许上传的图片后缀名 */
    protected $allowed_ext = ['jpg', 'png', 'gif', 'jpeg'];

    /**
     * 保存上传图片
     * @author: wuliang@koudailc.com
     *
     * @param UploadedFile    $file
     * @param                 $folder
     * @param                 $file_prefix
     * @param Boolean|integer $max_width
     *
     * @return array|bool
     */
    public function save(UploadedFile $file, $folder, $file_prefix, $max_width = false)
    {
        // 获取文件后缀名
        $extension = strtolower($file->getClientOriginalExtension()) ?: 'png';
        // 如果上传文件后缀名不允许，则终止
        if ( !in_array($extension, $this->allowed_ext) ) {
            return false;
        }

        // 构建文件存储的文件夹规则，例如：uploads/images/avatar/201808/11
        $folader_name = 'uploads/images/'.$folder.'/'.date('Ym/d', time());

        // 文件具体存储的物理路径，`public_path()` 获取的是`public`文件夹的物理地址
        $upload_path = public_path() . '/' .$folader_name;

        // 拼接文件名，加前缀是为了增加辨析度，前缀可以是相关数据模型的 ID
        // 值如：1_1493521050_7BVc9v9ujP.png
        $filename = $file_prefix .'_'. time() .'_'. str_random(10) .'.'. $extension;

        // 将文件移动到我们的目标文件夹中
        $file->move($upload_path, $filename);

        // 如果限制了图片宽度，就进行剪裁
        if ($max_width && $extension !== 'gif') {
            $this->reduceSize($upload_path . '/' . $filename, $max_width);
        }

        return [
            'path' => config('app.url') . "/$folader_name/$filename",
        ];
    }

    /**
     * 剪裁图片
     *
     * @author : wuliang@koudailc.com
     *
     * @param $file_path
     * @param $max_width
     */
    public function reduceSize($file_path, $max_width)
    {
        // 先实例化，传参是文件的磁盘物理路径
        $image = Image::make($file_path);

        // 对图片进行剪裁
        $image->resize($max_width, null, function (Constraint $constraint) {

            // 设定宽度是 $max_width，高度等比例双方缩放
            $constraint->aspectRatio();

            // 防止裁图时图片尺寸变大
            $constraint->upsize();

        });

        // 对图片修改后进行保存
        $image->save();
    }
}