<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * 跟新用户信息操作授权策略
     * 我们并不需要检查 $currentUser 是不是 NULL。未登录用户，框架会自动为其 所有权限 返回 false
     * 调用时，默认情况下，我们 不需要 传递当前登录用户至该方法内，因为框架会自动加载当前登录用户
     *
     * @author: wuliang@koudailc.com
     *
     * @param \App\Models\User $currenUser 当前登录用户
     * @param \App\Models\User $user       操作用户
     *
     * @return bool
     */
    public function update(User $currenUser, User $user)
    {
        return $currenUser->id === $user->id;
    }
}
