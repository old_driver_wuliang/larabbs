<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Reply;

class ReplyPolicy extends Policy
{
    /**
     * 回复删除权限
     * @author: wuliang@koudailc.com
     *
     * @param User  $user
     * @param Reply $reply
     *
     * @return bool
     */
    public function destroy(User $user, Reply $reply)
    {
        // 只有回复发表的作者或者话题的作者才可以删除回复
        return $user->isAuthorOf($reply) || $user->isAuthorOf($reply->topic);
    }
}
