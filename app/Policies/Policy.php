<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class Policy
{
    use HandlesAuthorization;

    public function __construct()
    {
        //
    }

    /**
     *
     * @param User $user
     * @param $ability
     *
     * @return bool
     */
    public function before($user, $ability)
	{
	    if ($user->can('manage_contents')) {
            return true;
        }
	}
}
