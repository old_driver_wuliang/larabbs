<?php

namespace App\Observers;

use App\Jobs\TranslateSlug;
use App\Models\Topic;

// creating, created, updating, updated, saving,
// saved,  deleting, deleted, restoring, restored
// 话题model观察器
class TopicObserver
{
    // 观察器，保存话题时触发
    public function saving(Topic $topic)
    {
        // XSS 过滤
        $topic->body = clean($topic->body, 'user_topic_body');

        // 生成话题摘录
        $topic->excerpt = make_excerpt($topic->body);
    }

    public function saved(Topic $topic)
    {
        // 如 slug 字段无内容，即使用翻译器对 title 进行翻译
        if ( !$topic->slug ) {
            // 推送任务到队列,因为队列对传入的 Eloquent 模型只序列化 ID 字段，
            // 所以分发任务时，需要保证 Eloquent 模型要有 ID
            // saving 观察器，中由于还未进行真正的创建，所以不能获取到 ID
            dispatch(new TranslateSlug($topic));
        }
    }

    public function deleted(Topic $topic)
    {
        // 删除话题时，同步删除该话题下的所有回复
        // 为避免再次触发删除回复的模型监听器，所以这里直接使用DB类进行操作
        \DB::table('replies')->where('topic_id', $topic->id)->delete();
    }
}