<?php

namespace App\Observers;

use App\Models\Reply;
use App\Notifications\TopicReplied;

// creating, created, updating, updated, saving,
// saved,  deleting, deleted, restoring, restored

class ReplyObserver
{
    public function creating(Reply $reply)
    {
        // Xss 过滤
        $reply->content = clean($reply->content, 'user_topic_body');
    }

    public function created(Reply $reply)
    {
        // 发布回复，增加对应帖子的回复数
        $reply->topic()->increment('reply_count');

        // 通知话题的作者，有了新的回复
        $reply->topic->user->notify(new TopicReplied($reply));
    }

    public function deleted(Reply $reply)
    {
        // 删除回复，减少对应帖子的回复数
        $reply->topic()->decrement('reply_count');
    }
}