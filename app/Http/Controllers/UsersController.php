<?php

namespace App\Http\Controllers;

use App\Handlers\ImageUploadHandler;
use App\Http\Requests\UserRequest;
use App\Models\User;

class UsersController extends Controller
{
    /**
     * UsersController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show']]);
    }

    /**
     * 用户个人信息展示页面
     *
     * @author: wuliang@koudailc.com
     *
     * @param \App\Models\User $user
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(User $user)
    {
        return view('users.show', compact('user'));
    }

    /**
     * 用户个人信息编辑页面
     *
     * @author: wuliang@koudailc.com
     *
     * @param \App\Models\User $user
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $user)
    {
        // 授权判断
        $this->authorize('update', $user);

        return view('users.edit', compact('user'));
    }

    /**
     * 用户个人信息修改数据保存
     *
     * @author: wuliang@koudailc.com
     *
     * @param \App\Http\Requests\UserRequest   $request
     * @param \App\Handlers\ImageUploadHandler $uploadHandler
     * @param \App\Models\User                 $user
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserRequest $request, ImageUploadHandler $uploadHandler, User $user)
    {
        // 授权判断
        $this->authorize('update', $user);

        $data = $request->all();

        if ($avatar = $request->file('avatar')) {
            $result = $uploadHandler->save($avatar, 'avatars', $user->id, 600);
            $data['avatar'] = $result['path'] ?: '';
        }

        $user->update($data);

        return redirect(route('users.show', $user->id))->with('success', '个人资料更新成功');
    }
}
