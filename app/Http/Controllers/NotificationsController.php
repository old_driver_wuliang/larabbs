<?php

namespace App\Http\Controllers;

use Illuminate\Notifications\DatabaseNotification;

class NotificationsController extends Controller
{

    /**
     * NotificationsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // 获取登录用户的所有通知
        /** @var DatabaseNotification $notifications */
        $notifications = \Auth::user()->notifications();
        $notifications = $notifications->paginate(20);

        // 通知标记为已读，未读数置为0
        \Auth::user()->markAsRead();

        return view('notifications.index', compact('notifications'));
    }
}
