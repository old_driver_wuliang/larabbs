<?php

namespace App\Http\Controllers;

use App\Models\Reply;
use App\Http\Requests\ReplyRequest;

class RepliesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * 创建回复
     *
     * @author: wuliang@koudailc.com
     *
     * @param ReplyRequest $request
     * @param Reply        $reply
     *
     * @return \Illuminate\Http\RedirectResponse
     */
	public function store(ReplyRequest $request, Reply $reply)
	{
		$reply->user_id = \Auth::id();
		$reply->topic_id = $request['topic_id'];
		$reply->content = $request['content'];

		if (!$reply->save()) {
            return redirect()->to($reply->topic->link())->with('danger', '回复发表失败');
        }

		return redirect()->to($reply->topic->link())->with('success', '回复发表成功');
	}

    /**
     * 删除回复
     *
     * @author: wuliang@koudailc.com
     *
     * @param Reply $reply
     *
     * @return \Illuminate\Http\RedirectResponse
     */
	public function destroy(Reply $reply)
	{
		$this->authorize('destroy', $reply);

		if (!$reply->delete()) {
            return redirect()->to($reply->topic->link())->with('danger', '回复删除失败');
        }

        return redirect()->to($reply->topic->link())->with('success', '回复删除成功');
	}
}