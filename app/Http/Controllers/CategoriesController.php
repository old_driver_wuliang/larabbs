<?php

namespace App\Http\Controllers;


use App\Models\Category;
use App\Models\Link;
use App\Models\Topic;
use App\Models\User;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * 分类帖子列表
     *
     * @author: wuliang@koudailc.com
     *
     * @param Category $category
     * @param Request  $request
     * @param Topic    $topic
     * @param User     $user
     * @param Link     $link
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Category $category, Request $request, Topic $topic, User $user, Link $link)
    {
        // 读取分类 ID 关联的话题，并按每 20 条分页
        $topics = $topic->withOrder($request->get('order'))->where(['category_id' => $category->id])->paginate();

        // 活跃用户列表
        $activeUsers = $user->getActiveUser();

        // 资源链接
        $links = $link->getAllCached();

        return view('topics.index', compact('topics', 'category', 'activeUsers', 'links'));
    }
}
