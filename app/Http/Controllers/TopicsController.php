<?php

namespace App\Http\Controllers;

use App\Handlers\ImageUploadHandler;
use App\Http\Requests\TopicRequest;
use App\Models\Category;
use App\Models\Link;
use App\Models\Topic;
use App\Models\User;
use Illuminate\Http\Request;

class TopicsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * 帖子列表
     *
     * @author: wuliang@koudailc.com
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Topic        $topic
     * @param \App\Models\User         $user
     * @param \App\Models\Link         $link
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Topic $topic, User $user, Link $link)
    {
        $topics = $topic->withOrder($request['order'])->paginate();
        $activeUsers = $user->getActiveUser();
        $links = $link->getAllCached();

        return view('topics.index', compact('topics', 'activeUsers', 'links'));
    }

    /**
     * 帖子信息
     *
     * @author: wuliang@koudailc.com
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Topic $topic
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request,Topic $topic)
    {
        // URL 矫正
        if ( !empty($topic->slug) && $topic->slug != $request['slug'] ) {
            return redirect($topic->link(), 301);
        }

        return view('topics.show', compact('topic'));
    }

    /**
     * 创建帖子页面
     *
     * @author: wuliang@koudailc.com
     *
     * @param \App\Models\Topic $topic
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function create(Topic $topic)
	{
	    $categories = Category::all();

		return view('topics.create_and_edit', compact('topic', 'categories'));
	}

    /**
     * 存储新建帖子信息
     *
     * @author: wuliang@koudailc.com
     *
     * @param TopicRequest $request
     * @param Topic        $topic
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(TopicRequest $request, Topic $topic)
    {
        $topic->fill($request->all());
        $topic->user_id = \Auth::id();
        if (!$topic->save()){
            return redirect()->route('topics.create', $topic->id)->with('danger', '新建话题失败');
        }

        return redirect()->to($topic->link())->with('success', '新建话题成功');
    }

    /**
     * 编辑帖子页面
     *
     * @author: wuliang@koudailc.com
     *
     * @param Topic $topic
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Topic $topic)
    {
        $this->authorize('update', $topic);

        $categories = Category::all();

        return view('topics.create_and_edit', compact('topic', 'categories'));
    }

    /**
     * 更新帖子信息
     *
     * @author: wuliang@koudailc.com
     *
     * @param TopicRequest $request
     * @param Topic        $topic
     *
     * @return \Illuminate\Http\RedirectResponse
     */
	public function update(TopicRequest $request, Topic $topic)
	{
		$this->authorize('update', $topic);
		$topic->update($request->all());

		return redirect()->to($topic->link())->with('success', '更新成功.');
	}

    /**
     * 删除帖子
     *
     * @author: wuliang@koudailc.com
     *
     * @param Topic $topic
     *
     * @return \Illuminate\Http\RedirectResponse
     */
	public function destroy(Topic $topic)
	{
		$this->authorize('destroy', $topic);
		$topic->delete();

		return redirect()->route('topics.index')->with('success', '删除成功.');
	}

    /**
     * 保存上传图片
     *
     * @author: wuliang@koudailc.com
     *
     * @param Request            $request
     * @param ImageUploadHandler $imageUploadHandler
     *
     * @return array
     */
	public function uploadImage(Request $request, ImageUploadHandler $imageUploadHandler)
    {
        // 初始化返回数据，默认失败
        $data = [
            'success'   => false,
            'msg'       => '上传失败',
            'file_path' => '',
        ];

        if ($file = $request->file('upload_file')) {
            $result = $imageUploadHandler->save($file, 'topics', \Auth::id(), 1024);
            if ($result) {
                $data = [
                    'success'   => true,
                    'msg'       => '上传成功',
                    'file_path' => $result['path'],
                ];
            }
        }

        return $data;
    }
}