<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => 'required|between:3,25|regex:/^[A-Za-z0-9\-\_]+$/|unique:users,name,'.\Auth::id(),
            'email'        => 'required|email',
            'introduction' => 'max:80',
            'avatar'       => 'mimes:jpeg,jpg,gif,png|dimensions:min_widtth=300,min_height=300',
        ];
    }

    public function messages()
    {
        return [
            'name.unique'   => '用户名已存在，请重新填写',
            'name.required' => '用户名不能为空',
            'name.between'  => '用户名只能介于 3-25 个字符之间',
            'name.regex'    => '用户名只能是数字、英文字符、横线和下划线',
            'avatar.mimes'  => '用户头像必须是 jpeg,jpg,gif,png 格式',
            'avatar.dimensions' => '用户头像清晰度不够，宽度和高度必须在300px以上'
        ];
    }
}
