<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * @package App\Models
 *
 * @property integer $id
 * @property string  $name
 * @property string  $description
 * @property integer $post_count
 * @property string  $created_at
 * @property string  $updated_at
 */
class Category extends Model
{
    protected $fillable = ['name', 'description'];
}
