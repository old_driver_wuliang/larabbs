<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class Topic
 * @package App\Models
 *
 * @property integer  $id
 * @property string   $title
 * @property string   $body
 * @property integer  $user_id
 * @property integer  $category_id
 * @property integer  $reply_count
 * @property integer  $view_count
 * @property integer  $last_reply_user_id
 * @property integer  $order
 * @property string   $excerpt
 * @property string   $slug
 * @property string   $created_at
 * @property string   $updated_at
 *
 * @property Category $category
 * @property User     $user
 * @property Reply    $replies
 */
class Topic extends Model
{
    protected $fillable = ['title', 'body', 'category_id', 'excerpt', 'slug'];

    /**
     * 排序Scope
     *
     * @author: wuliang@koudailc.com
     *
     * @param Builder $query
     * @param         $order
     *
     * @return Builder
     */
    public function scopeWithOrder(Builder $query, $order)
    {
        // 不同的排序，使用不同的数据读取逻辑
        switch ($order) {
            case 'recent':
                $query->recent();
                break;

            default:
                $query->recentReplied();
                break;
        }
        // 预加载防止 N+1 问题
        return $query->with('user', 'category');
    }

    /**
     * 按最新回复排序
     *
     * @author: wuliang@koudailc.com
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeRecentReplied(Builder $query)
    {
        // 当话题有新回复时，我们将编写逻辑来更新话题模型的 reply_count 属性，
        // 此时会自动触发框架对数据模型 updated_at 时间戳的更新
        return $query->orderBy('updated_at', 'desc');
    }

    /**
     * 按最新发布排序
     *
     * @author: wuliang@koudailc.com
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeRecent(Builder $query)
    {
        // 按照创建时间排序
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * 获取话题对应的分类
     *
     * @author: wuliang@koudailc.com
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * 获取话题对应的作者
     *
     * @author: wuliang@koudailc.com
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * 获取话题下对应的所有回复
     *
     * @author: wuliang@koudailc.com
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    /**
     * 生成话题访问url
     *
     * @author: wuliang@koudailc.com
     *
     * @param array $param
     *
     * @return string
     */
    public function link($param = [])
    {
        return route('topics.show', array_merge([$this->id, $this->slug], $param));
    }
}
