<?php

namespace App\Models;

/**
 * Class Reply
 * @package App\Models
 *
 * @property integer $id
 * @property integer $topic_id
 * @property integer $user_id
 * @property string  $content
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Topic   $topic
 * @property User    $user
 */
class Reply extends Model
{
    protected $fillable = ['content'];

    /**
     * 获取回复所属的话题
     *
     * @author: wuliang@koudailc.com
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function topic()
    {
        return $this->belongsTo(Topic::class);
    }

    /**
     * 获取回复的所有者
     *
     * @author: wuliang@koudailc.com
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
