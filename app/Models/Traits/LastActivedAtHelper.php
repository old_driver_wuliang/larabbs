<?php
namespace App\Models\Traits;

use Carbon\Carbon;

trait LastActivedAtHelper
{
    // 缓存相关,用户最后活跃时间
    protected $hash_prefix = 'larabbs_last_actived_at_';
    protected $field_prefix = 'user_';

    /**
     * 访问器，获取最后登录时间
     *
     * @author: wuliang@koudailc.com
     *
     * @param $value
     *
     * @return Carbon
     */
    public function getLastActivedAtAttribute($value)
    {
        // 获取今天的日期
        $date = Carbon::now()->toDateString();

        // Redis 哈希表的命名，如：larabbs_last_actived_at_2017-10-21
        $hashKey = $this->getHashFromDateString($date);

        // 字段名称，如：user_1
        $filed = $this->getHashField();

        $datetime = \Redis::hGet($hashKey, $filed) ?: $value;

        // 如果存在的话，返回时间对应的 Carbon 实体
        // 否则使用用户注册时间
        if ($datetime) {
            return new Carbon($datetime);
        } else {
            return $this->created_at;
        }
    }

    /**
     * 缓存用户最后登录时间
     *
     * @author : wuliang@koudailc.com
     */
    public function recordLastActivedAt()
    {
        // 获取今天的日期
        $date = Carbon::now()->toDateString();

        // Redis 哈希表的命名，如：larabbs_last_actived_at_2017-10-21
        $hashKey = $this->getHashFromDateString($date);

        // 字段名称，如：user_1
        $filed = $this->getHashField();

        // 当前时间，如：2017-10-21 08:35:15
        $nowTime = Carbon::now()->toDateTimeString();

        // 数据写入 Redis ，字段已存在会被更新
        \Redis::hSet($hashKey, $filed, $nowTime);
    }

    /**
     * 同步缓存中用户最后登录时间
     *
     * @author : wuliang@koudailc.com
     */
    public function syncUserActivedAt()
    {
        // 获取昨天的日期
        $yesterdayDate = Carbon::yesterday()->toDateString();

        $hashKey = $this->getHashFromDateString($yesterdayDate);

        // 从 Redis 中获取所有哈希表里的数据
        $datas = \Redis::hGetAll($hashKey);

        foreach ($datas as $userId => $activedAt) {
            // 会将 `user_1` 转换为 1
            $userId = str_replace($this->field_prefix, '', $userId);

            // 只有当用户存在时才更新到数据库中
            if ($user = $this->find($userId)) {
                $user->last_actived_at = $activedAt;
                $user->save();
            }
        }

        // 以数据库为中心的存储，既已同步，即可删除
        \Redis::del($hashKey);
    }

    /**
     * 获取缓存数据的key
     *
     * @author: wuliang@koudailc.com
     *
     * @param $date
     *
     * @return string
     */
    public function getHashFromDateString($date)
    {
        // Redis 哈希表的命名，如：larabbs_last_actived_at_2017-10-21
        return $this->hash_prefix . $date;
    }

    /**
     * 获取缓存字段
     *
     * @author: wuliang@koudailc.com
     * @return string
     */
    public function getHashField()
    {
        // 字段名称，如：user_1
        return $this->field_prefix . $this->id;
    }
}