<?php

namespace App\Models\Traits;

use App\Models\Reply;
use App\Models\Topic;
use Carbon\Carbon;

trait ActiveUserHelper
{
    // 用于存放临时用户数据
    protected $users = [];

    // 配置信息
    protected $topic_weight = 4; // 话题权重
    protected $reply_weight = 1; // 回复权重
    protected $pass_days = 7;    // 多少天内发表过内容
    protected $user_number = 6; // 取出来多少用户

    // 缓存相关配置
    protected $cache_key = 'larabbs_active_users';
    protected $cache_expire_in_minutes = 65;

    /**
     * 获取活跃用户
     *
     * @author: wuliang@koudailc.com
     * @return mixed
     */
    public function getActiveUser()
    {
        // 尝试从缓存中取出 cache_key 对应的数据。如果能取到，便直接返回数据。
        // 否则运行匿名函数中的代码来取出活跃用户数据，返回的同时做了缓存。
        return \Cache::remember($this->cache_key, $this->cache_expire_in_minutes, function () {
            return $this->calculateActiveUsers();
        });
    }


    /**
     * 取得活跃用户列表并写入缓存
     *
     * @author : wuliang@koudailc.com
     */
    public function calculateAndCacheActiveUsers()
    {
        // 取得活跃用户列表
        $activeUsers = $this->calculateActiveUsers();

        // 将活跃用户写入缓存
        $this->cacheActiveUsers($activeUsers);
    }

    /**
     * 计算活跃用户
     *
     * @author: wuliang@koudailc.com
     * @return \Illuminate\Support\Collection
     */
    public function calculateActiveUsers()
    {
        $this->calculateTopicScore();
        $this->calculateReplyScore();

        // 数组按照得分排序
        $users = array_sort($this->users, function ($user) {
            return $user['score'];
        });

        // 我们需要的是倒序，高分靠前，第二个参数为保持数组的 KEY 不变
        $users = array_reverse($users, true);

        // 只获取我们想要的数量
        $users = array_slice($users, 0, $this->user_number, true);

        // 新建一个空集合
        $activeUsers = collect();

        foreach ($users as $userId => $user){
            if ($user = $this->find($userId)) {
                $activeUsers->push($user);
            }
        }

        // 返回数据
        return $activeUsers;
    }

    /**
     * 计算用户发布帖子的分数
     *
     * @author : wuliang@koudailc.com
     */
    private function calculateTopicScore()
    {
        // 从话题数据表里取出限定时间范围（$pass_days）内，有发表过话题的用户
        // 并且同时取出用户此段时间内发布话题的数量
        $topicUsers = Topic::query()->select(\DB::raw('user_id, count(id) as topic_count'))
            ->where('created_at', '>=', Carbon::now()->subDays($this->pass_days))
            ->groupBy('user_id')->get();

        // 根据话题数量计算得分
        foreach ($topicUsers as $value) {
            $this->users[$value->user_id]['score'] = $value->topic_count * $this->topic_weight;
        }
    }

    /**
     * 计算用户回复帖子的分数
     *
     * @author : wuliang@koudailc.com
     */
    private function calculateReplyScore()
    {
        // 从回复数据表里取出限定时间范围（$pass_days）内，有发表过回复的用户
        // 并且同时取出用户此段时间内发布回复的数量
        $replyUsers = Reply::query()->select(\DB::raw('user_id, count(id) as reply_count'))
            ->where('created_at', '>=', Carbon::now()->subDays($this->pass_days))
            ->groupBy('user_id')->get();

        // 根据回复数量计算得分
        foreach ($replyUsers as $value) {
            $replyScore = $value->reply_count * $this->reply_weight;
            if (isset($this->users[$value->user_id])) {
                $this->users[$value->user_id]['score'] += $replyScore;
            } else {
                $this->users[$value->user_id]['score'] = $replyScore;
            }
        }
    }

    /**
     * 将活跃用户写入缓存
     *
     * @author : wuliang@koudailc.com
     *
     * @param $activeUsers
     */
    private function cacheActiveUsers($activeUsers)
    {
        \Cache::put($this->cache_key, $activeUsers, $this->cache_expire_in_minutes);
    }
}