<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model as EloquentModel;

class Model extends EloquentModel
{
    public function scopeRecent(Builder $query)
    {
        return $query->orderBy('id', 'desc');
    }

    public function scopeOrdered(Builder $query)
    {
        return $query->orderBy('order', 'desc');
    }

}
