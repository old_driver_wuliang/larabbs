<?php

namespace App\Models;

use App\Models\Traits\ActiveUserHelper;
use App\Models\Traits\LastActivedAtHelper;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class User
 * @package App\Models
 *
 * @property integer $id
 * @property string  $name
 * @property string  $email
 * @property string  $password
 * @property string  $remember_token
 * @property string  $created_at
 * @property string  $updated_at
 * @property string  $avatar
 * @property string  $introduction
 * @property integer $notification_count
 * @property string  $last_actived_at
 *
 * @property array   $attributes
 *
 * @property Topic   $topics
 * @property Reply   $replies
 */
class User extends Authenticatable
{
    use Notifiable {notify as protected laravelNotify; }
    use HasRoles; // 让我们获取到扩展包提供的所有权限和角色的操作方法
    use ActiveUserHelper;
    use LastActivedAtHelper;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'avatar', 'introduction',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * 修改器：后台设置密码时，对密码做加密处理
     *
     * @author : wuliang@koudailc.com
     *
     * @param $value
     *
     */
    public function setPasswordAttribute($value)
    {
        // 如果值的长度等于 60，即认为是已经做过加密的情况
        if (strlen($value) != 60) {

            // 不等于 60，做密码加密处理
            $value = bcrypt($value);
        }

        $this->attributes['password'] = $value;
    }

    /**
     * 修改器：后台上传头像是，补全url
     *
     * @author : wuliang@koudailc.com
     *
     * @param $path
     */
    public function setAvatarAttribute($path)
    {
        // 如果不是 `http` 子串开头，那就是从后台上传的，需要补全 URL
        if ( ! starts_with($path, 'http')) {

            // 拼接完整的 URL
            $path = config('app.url') . "/uploads/images/avatars/$path";
        }

        $this->attributes['avatar'] = $path;
    }

    /**
     * 用户操作权限判断
     *
     * @author: wuliang@koudailc.com
     *
     * @param $model
     *
     * @return bool
     */
    public function isAuthorOf($model)
    {
        return $this->id == $model->user_id;
    }

    /**
     * 获取用户发布的话题信息
     *
     * @author: wuliang@koudailc.com
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function topics()
    {
        return $this->hasMany(Topic::class);
    }

    /**
     * 获取用户所有的回复
     *
     * @author: wuliang@koudailc.com
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    /**
     * 重写通知方法
     *
     * @author: wuliang@koudailc.com
     *
     * @param $instance
     *
     * @return bool
     */
    public function notify($instance)
    {
        // 如果要通知的人是当前登录用户，就不必通知了
        if ($this->id == \Auth::id()) {
            return true;
        }

        // 增加用户收到的未读通知数
        $this->increment('notification_count');

        // 调用系统通知方法
        $this->laravelNotify($instance);

        return true;
    }

    /**
     * 清空未读通知数 && 将通知状态置为已读
     *
     * @author : wuliang@koudailc.com
     * @return mixed
     */
    public function markAsRead()
    {
        $this->notification_count = 0;
        $this->save();

        $this->unreadNotifications->markAsRead();
    }
}
